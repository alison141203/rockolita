function loadDataGeneros() {
    let request = sendRequest('generos/list', 'GET', '')
    let table = document.getElementById('tabla-generos');
    table.innerHTML = "";
    request.onload = function () {
        let data = request.response;
        data.forEach(element => {
            table.innerHTML += `
                <tr>
                    <th>${element.idgeneros}</th>
                    <td>${element.nombregeneros}</td>
                    <td>${element.tipogeneros}</td>
                    <td>${element.categorias}</td>
                    <td>
                        <div class= "form-check form switch">
                            <input ${element.activo ? "checked" : "unchecked"}
                            class= "form-check-input" type="checkbox" role="switch" disabled>
                        </div>
                    </td>
                    <td>
                        <button type= "button" class="btn btn-primary"
                        onclick='window.location = "/formgeneros.html?id=${element.idgeneros}"'>Editar</button>
                        <button type= "button" class= "btn btn-danger"
                        onclick= 'deleteGeneros(${element.idgeneros})'>Eliminar</button>
                    </td>
                </tr>
                `                
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="5">Error al recuperar los datos.</td>
            </tr>
        `;        
    }
}
function loadGeneros(idgeneros){
    let request = sendRequest('generos/list/'+idgeneros,'GET','')
    let idgeneros = document.getElementById('idgeneros')
    let nombregeneros = document.getElementById('nombregeneros')
    let tipogeneros = document.getElementById('tipogeneros')
    let categorias = document.getElementById('categorias')
    
    request.onload = function(){
         let data = request.response
          idgeneros.value = data.idgeneros
          nombregeneros.value = data.nombregeneros
          tipogeneros.value = data.tipogeneros
          categorias.value = data.categorias
    }
     
    request.onerror = function(){
          alert('Error al recuperar los datos de la cancion');
    };
}

function deleteGeneros(idgeneros){
    let request = sendRequest('generos/'+idgeneros,'DELETE','');
    request,onload = function(){
        loadDataGeneros();
    }
}

function saveGeneros(){
    let nombregeneros = document.getElementById('nombregeneros').value;
    let tipogeneros = document.getElementById('tipogeneros').value;
    let categorias= document.getElementById('categorias').value;
    let data = {
        'idgeneros':idgeneros,
        'nombregeneros':nombregeneros , 'tipogeneros':tipogeneros,
        'categorias':categorias
    }
    let request = sendRequest('generos/', idgeneros ? 'PUT': 'POST', data);
    request.onload = function (){
         window.location = 'generos.html';
    };
    request.onerror = function(){
         alert('Error al guardar la cancion');
    };
}