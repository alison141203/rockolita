function loadDataCanciones() {
    let request = sendRequest('canciones/list', 'GET', '')
    let table = document.getElementById('tabla-canciones');
    table.innerHTML = "";
    request.onload = function () {
        let data = request.response;
        data.forEach(element => {
            table.innerHTML += `
                <tr>
                    <th>${element.idcanciones}</th>
                    <td>${element.nombrecanciones}</td>
                    <td>${element.idartistas}</td>
                    <td>${element.idgeneros}</td>
                    <td>${element.anio}</td>
                    <td>
                        <div class= "form-check form switch">
                            <input ${element.activo ? "checked" : "unchecked"}
                            class= "form-check-input" type="checkbox" role="switch" disabled>
                        </div>
                    </td>
                    <td>
                        <button type= "button" class="btn btn-primary"
                        onclick='window.location = "/formcanciones.html?id=${element.idcanciones}"'>Editar</button>
                        <button type= "button" class= "btn btn-danger"
                        onclick= 'deleteCancion(${element.idcanciones})'>Eliminar</button>
                    </td>
                </tr>
                `                
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
        `;        
    }
}
function loadCanciones(idcanciones){
    let request = sendRequest('canciones/list/'+idcanciones,'GET','')
    let idcanciones = document.getElementById('idcanciones')
    let nombrecanciones = document.getElementById('nombrecanciones')
    let idartistas = document.getElementById('idartistas')
    let idgeneros = document.getElementById('idgeneros')
    let anio = document.getElementById('anio')
    
    request.onload = function(){
         let data = request.response
          idcanciones.value = data.idcanciones
          nombrecanciones.value = data.nombrecanciones
          idartistas.value = data.idartistas
          idgeneros.value = data.idgeneros
          anio.value = data.anio
    }
     
    request.onerror = function(){
          alert('Error al recuperar los datos de la cancion');
    };
}

function deleteCancion(idcanciones){
    let request = sendRequest('canciones/'+idcanciones,'DELETE','');
    request,onload = function(){
        loadDataCanciones();
    }
}

function saveCancion(){
    let nombrecanciones = document.getElementById('nombrecanciones').value;
    let idartistas = document.getElementById('idartistas').value;
    let idgeneros = document.getElementById('idgeneros').value;
    let anio = document.getElementById('anio').value;
    let data = {
        'idcanciones':idcanciones,
        'nombrecanciones':nombrecanciones , 'idartistas':idartistas,
        'idgeneros':idgeneros , 'anio':anio
    }
    let request = sendRequest('canciones/', idcanciones ? 'PUT': 'POST', data);
    request.onload = function (){
         window.location = 'canciones.html';
    };
    request.onerror = function(){
         alert('Error al guardar la cancion');
    };
}

