/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.rockola.modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
//generos
@Entity
@Table(name = "generos")
public class Generos implements Serializable {

//    @Autowired
//    transient JdbcTemplate jdbcTemplate;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idgeneros")
    private Integer idgeneros;
    @Column(name = "nombregenero")
    private String nombregenero;
    @Column(name = "tipogenero")
    private String tipogenero;
    @Column(name = "categorias")
    private String categorias;

    public Integer getIdgeneros() {
        return idgeneros;
    }

    public void setIdgeneros(Integer idgeneros) {
        this.idgeneros = idgeneros;
    }

    public String getNombregenero() {
        return nombregenero;
    }

    public void setNombregenero(String nombregenero) {
        this.nombregenero = nombregenero;
    }

    public String getTipogenero() {
        return tipogenero;
    }

    public void setTipogenero(String tipogenero) {
        this.tipogenero = tipogenero;
    }

    public String getCategorias() {
        return categorias;
    }

    public void setCategorias(String categorias) {
        this.categorias = categorias;
    }
    
    
}
