/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.rockola.modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
//canciones
@Entity
@Table(name="canciones")
public class Canciones implements Serializable{
//    @Autowired
//    transient JdbcTemplate jdbcTempate;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="idcanciones")
    private Integer idcanciones;
    @Column(name="nombrecanciones")
    private String nombrecanciones;
    @ManyToOne
    @JoinColumn(name="artistas_idartista")
    private Artistas idartistas;
    @ManyToOne
    @JoinColumn(name="generos_idgeneros")
    private Generos idgeneros;
    @Column(name="anio")
    private Integer anio;

    public Integer getIdcanciones() {
        return idcanciones;
    }

    public void setIdcanciones(Integer idcanciones) {
        this.idcanciones = idcanciones;
    }

    public String getNombrecanciones() {
        return nombrecanciones;
    }

    public void setNombrecanciones(String nombrecanciones) {
        this.nombrecanciones = nombrecanciones;
    }

    public Artistas getIdartistas() {
        return idartistas;
    }

    public void setIdartistas(Artistas idartistas) {
        this.idartistas = idartistas;
    }

    public Generos getIdgeneros() {
        return idgeneros;
    }

    public void setIdgeneros(Generos idgeneros) {
        this.idgeneros = idgeneros;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }
    
    
    
    

    
}
