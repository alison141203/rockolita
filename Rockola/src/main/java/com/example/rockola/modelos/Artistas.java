/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.rockola.modelos;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
//Artistas
@Entity
@Table(name="artistas")
public class Artistas implements Serializable{
//    @Autowired
//    transient JdbcTemplate jdbcTemplate; // paquete que nos permite ejecutar la consulta sql
    // puede que no siempre se ejecute la consulta
    // atributos
    
    //ATRIBUTOS
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idartista")
    private Integer idartista ;
    
    @Column(name="nombreartista")
    private String nombreartista;
    
    @Column(name="paisartista")
    private String paisartista;

    public Integer getIdartista() {
        return idartista;
    }

    public void setIdartista(Integer idartista) {
        this.idartista = idartista;
    }

    public String getNombreartista() {
        return nombreartista;
    }

    public void setNombreartista(String nombreartista) {
        this.nombreartista = nombreartista;
    }

    public String getPaisartista() {
        return paisartista;
    }

    public void setPaisartista(String paisartista) {
        this.paisartista = paisartista;
    }
    
   //Artistas 
}