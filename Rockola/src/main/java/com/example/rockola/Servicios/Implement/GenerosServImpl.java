/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.rockola.Servicios.Implement;

import com.example.rockola.Dao.GenerosDao;
import com.example.rockola.Servicios.GenerosServices;
import com.example.rockola.modelos.Generos;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Dani
 */
@Service
public class GenerosServImpl implements GenerosServices {
    
     @Autowired
    private GenerosDao generosdao;

    @Override
    @Transactional(readOnly=false)
    public Generos save(Generos generos) {
        return generosdao.save(generos);
    }

    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id) {
        generosdao.deleteById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Generos findById(Integer id) {
        return generosdao.findById(id).orElse(null);
    }

    /**
     *
     * @return
     */
    @Override
    @Transactional(readOnly=true)
    public List<Generos> findAll() {
        return (List<Generos>) generosdao.findAll();
    }
}
