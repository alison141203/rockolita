/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.rockola.Servicios;

import com.example.rockola.modelos.Artistas;
import java.util.List;

/**
 *
 * @author Dani
 */
public interface ArtistasService {
    public Artistas save(Artistas artista);
    public void delete(Integer id);
    public Artistas findById(Integer id);
    public List<Artistas> findAll();
}
