/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.rockola.Servicios;

import com.example.rockola.modelos.Canciones;
import java.util.List;

/**
 *
 * @author Dani
 */
public interface CancionesServices {
    public Canciones save(Canciones canciones);
    public void delete(Integer id);
    public Canciones findById(Integer id);
    public List<Canciones>findAll();
}
