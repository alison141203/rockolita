/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.rockola.Servicios;

import com.example.rockola.modelos.Generos;
import java.util.List;

/**
 *
 * @author Dani
 */
public interface GenerosServices {
    public Generos save(Generos generos);
    public void delete(Integer id);
    public Generos findById(Integer id);
    public List<Generos>findAll();
}
