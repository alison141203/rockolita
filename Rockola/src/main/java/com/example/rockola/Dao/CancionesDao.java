/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.rockola.Dao;

import com.example.rockola.modelos.Canciones;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Dani
 */
public interface CancionesDao extends CrudRepository<Canciones, Integer>{
    
}
