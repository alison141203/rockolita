/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.rockola.Dao;

import com.example.rockola.modelos.Artistas;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Dani
 */
public interface ArtistaDao extends CrudRepository<Artistas, Integer> {
    
}
