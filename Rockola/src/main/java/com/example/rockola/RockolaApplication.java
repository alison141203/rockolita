package com.example.rockola;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages={"com.example.rockola.Controller"})
public class RockolaApplication {

	public static void main(String[] args) {
		SpringApplication.run(RockolaApplication.class, args);
	}

}
