/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.rockola.Controller;

import com.example.rockola.Servicios.CancionesServices;
import com.example.rockola.modelos.Canciones;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping(path="/canciones")
public class CancionesController {
        @Autowired
    private CancionesServices cancionesservices;

    @PostMapping( "/")
    public ResponseEntity<Canciones> agregar(@RequestBody Canciones canciones) {
        Canciones obj = cancionesservices.save(canciones);

        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Canciones> eliminar(@PathVariable Integer id) {
        Canciones obj = cancionesservices.findById(id);

        if (obj != null) {
            cancionesservices.delete(id);
        } 
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping("/")
    public ResponseEntity<Canciones> editar(@RequestBody Canciones canciones) {
        Canciones obj = cancionesservices.findById(canciones.getIdcanciones());
        if (obj != null) {
            obj.setNombrecanciones(canciones.getNombrecanciones());
            obj.setIdgeneros(canciones.getIdgeneros());
            obj.setIdartistas(canciones.getIdartistas());
            obj.setAnio(canciones.getAnio());
            cancionesservices.save(obj);
        } 
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Canciones> consultarTodo() {
        return cancionesservices.findAll();
    }

    @GetMapping("/")
    public String saludar(){
        return "Hola desde transaccion";
    }
    
    @GetMapping("/list/{id}")
    public Canciones consultarPorId(@PathVariable Integer id) {
        return cancionesservices.findById(id);
    }
    
}
