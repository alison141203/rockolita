/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.rockola.Controller;

import com.example.rockola.Servicios.ArtistasService;
import com.example.rockola.modelos.Artistas;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin("*")
@RequestMapping(path="/artistas")
public class ArtistasController {
    
        @Autowired
    private ArtistasService artistaService;

    @PostMapping ("/")
public ResponseEntity<Artistas>agregar(@RequestBody Artistas artistas){
    Artistas obj=artistaService.save(artistas);
    
    return new ResponseEntity<>(obj, HttpStatus.OK);
}
@DeleteMapping ("/{id}")
public ResponseEntity<Artistas>eliminar(@PathVariable Integer id){
    Artistas obj=artistaService.findById(id);
    
    if (obj!=null){
        artistaService.delete(id);
      }
    
       return new ResponseEntity<>(obj, HttpStatus.OK);  
}
@PutMapping ("/")
public ResponseEntity<Artistas>editar(@RequestBody Artistas artistas){
    Artistas obj=artistaService.findById(artistas.getIdartista());
    if (obj!=null)
    {
        obj.setNombreartista(artistas.getNombreartista());
        obj.setPaisartista(artistas.getPaisartista());
        artistaService.save(obj);
    }
   
        return new ResponseEntity<>(obj, HttpStatus.OK);      
}
@GetMapping("/saludo")  
    public String saludo(){
        return "hola";
    }
  
  

    @GetMapping("/list")
    public List<Artistas> consultarTodo() {
        return artistaService.findAll();
    }

    @GetMapping("/list/{id}")
    public Artistas consultarPorId(@PathVariable Integer id) {
        return artistaService.findById(id);
    }
    
}
